
> 项目地址: https://gitee.com/vizo/vio-utils

## Usage

``` js
<script src="https://unpkg.com/vio-utils@latest"></script>
or
import vio from 'vio-utils'
```


``` js
// all
vio.isNum
vio.getEl
vio.randItem
vio.randNum
vio.randStr
vio.shuffle
vio.randArr
vio.randColor
vio.randCname
vio.uniqArr
vio.throttle
vio.debounce
vio.setStore
vio.getStore
vio.round
vio.roundFO
vio.floor
vio.floorFO
vio.ceil
vio.ceilFO
vio.isEqual
await vio.timeout
vio.copyTextOld
vio.copyText
vio.downloadText
vio.downloadUrl
vio.fmt
vio.toThousands
vio.deepClone
vio.fmtNumToCN
await vio.imgToBase64
vio.isUnique
vio.chunk
vio.chunkIntoN
vio.getType
vio.vibrate
vio.intersection
vio.matches
vio.isBottom
vio.scrollTo
vio.getKeybyVal
vio.evalFunc
vio.isMobile
vio.isAndroid
vio.isIOS
vio.isWx
vio.hexToRgb
vio.rgbToHex
vio.isIdCard
vio.getBirthById
vio.isIpv4
vio.prob
vio.uuid
vio.urlParams
vio.rndS
vio.rndN
vio.dayOfYear
vio.deferCall
vio.stripHtml
vio.equals
vio.once
vio.log
vio.watchDom
vio.has
vio.fmtBytes
vio.gid
await vio.sha256
vio.isEmpty
vio.isIterable
vio.objToArr
vio.arrToObj
vio.group
vio.count
vio.setSessionStore
vio.getSessionStore

// 2.10.0 add
await vio.sleep
vio.isEqual
vio.isEmpty
vio.isIterable
vio.objToArr
vio.arrToObj
vio.group
vio.count

// 2.11.0 add
vio.setSessionStore
vio.getSessionStore

```

