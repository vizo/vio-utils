const vio  = require('../index')


// expect(2 + 2).toBe(4)
// expect(2 + 2).not.toBe(5)
// expect(data).toEqual({one: 1, two: 2})
// expect(arr).toContain('xx')
// expect(() => throwErr()).toThrow()
// .toBeTruthy()
// .toBeFalsy()
// .toMatch(/\d+/)

test('isNum', () => {
  expect( vio.isNum(2) ).toBeTruthy()
})

test('randNum', () => {
  // 返回的一定是数值
  expect( vio.isNum( vio.randNum(20) ) ).toBeTruthy()
})

test('rndS', () => {
  expect( vio.rndS(20) ).toMatch(/^[a-zA-Z\d]{20}$/)
  expect( vio.rndS(20, 'un') ).toMatch(/^[A-Z\d]{20}$/)
  expect( vio.rndS(20, 'nl') ).toMatch(/^[a-z\d]{20}$/)
})

test('shuffle', () => {
  const ar1 = Array.from({length: 1e3}, v => vio.rndS(10))
  const ar2 = vio.shuffle(ar1)
  // ar1一定不等于ar2
  expect(ar1).not.toEqual(ar2)
  // ar2一定是数组
  expect(Array.isArray(ar2)).toBeTruthy()
})

test('round', () => {
  expect( vio.round('10.567') ).toBe(10.57)
})

test('roundFO', () => {
  expect( vio.roundFO('10.56', 3) ).toEqual('10.560')
})

test('floor', () => {
  expect( vio.floor('10.567', 2) ).toEqual(10.56)
})

test('floorFO', () => {
  expect( vio.floorFO('10.56', 3) ).toEqual('10.560')
})

test('ceil', () => {
  expect( vio.ceil('10.521', 2) ).toEqual(10.53)
})

test('ceilFO', () => {
  expect( vio.ceilFO('10.5611', 3) ).toEqual('10.562')
})

test('fmt', () => {
  expect( vio.fmt(Date.now(), 'Y-M-D') ).toMatch(/^\d{4}-\d{2}-\d{2}$/)
  expect( vio.fmt(Date.now(), 'h:m:s') ).toMatch(/^\d{2}:\d{2}:\d{2}$/)
})

test('deepClone', () => {
  const o1 = {
    a: 1,
    b: () => {},
    c: /\d$/igu,
    d: new Date()
  }
  const o2 = vio.deepClone(o1)
  expect(o1).toEqual(o2)
})

test('fmtNumToCN', () => {
  expect(vio.fmtNumToCN(10000000000)).toEqual('100亿')
})

test('isUnique', () => {
  expect(vio.isUnique([1, 2, 3, 3])).toBeFalsy()
})

test('chunk', () => {
  expect(vio.chunk([1, 2, 3, 4, 5, 6, 7], 2).length === 4 ).toBeTruthy()
})

test('chunkIntoN', () => {
  expect(vio.chunkIntoN([1, 2, 3, 4, 5, 6, 7], 3).length === 3 ).toBeTruthy()
})

test('matches', () => {
  const o1 = {a: 1, b: 2}
  const o2 = {b: 2}
  expect(vio.matches(o1, o2)).toBeTruthy()
})

test('getKeyByVal', () => {
  const o1 = {a: 1, b: 2}
  expect(vio.getKeyByVal(o1, 2)).toEqual('b')
})

test('rgbToHex', () => {
  expect(vio.rgbToHex('rgb(250,47,95)')).toMatch(/^#[a-f0-9]{6}$/)
})

test('isIdCard', () => {
  expect(vio.isIdCard('110101200001021780')).toBeTruthy()
})

test('equals', () => {
  const o1 = {a: {b: 2}}
  const o2 = {a: {b: 2}}
  expect(vio.equals(o1, o2)).toBeTruthy()
})

test('isEqual', () => {
  const o1 = {a: {b: 2}}
  const o2 = {a: {b: 2}}
  expect(vio.isEqual(o1, o2)).toBeTruthy()
})

test('has', () => {
  const o = {a: 1}
  const b = vio.has(o, 'a')
  expect(b).toBeTruthy()
})

test('fmtBytes', () => {
  expect( vio.fmtBytes(4985 ** 2, 2) ).toEqual('23.69MB')
})

test('gid', () => {
  expect( vio.gid(80).length === 80 ).toBeTruthy()
})

test('isEmpty', () => {
  expect( vio.isEmpty({}) ).toBeTruthy()
  expect( vio.isEmpty([]) ).toBeTruthy()
  expect( vio.isEmpty(null) ).toBeTruthy()
  expect( !vio.isEmpty(0) ).toBeTruthy()
})

test('isIterable', () => {
  expect( vio.isIterable({}) ).toBeFalsy()
  expect( vio.isIterable([]) ).toBeTruthy()
})

test('objToArr', () => {
  const o1 = {k1: 'v1', k2: 'v2'}
  expect( vio.objToArr(o1, 'tit', 'cnt') ).toEqual([
    {tit: 'k1', cnt: 'v1'},
    {tit: 'k2', cnt: 'v2'},
  ])
})

test('group', () => {
  const arr = [
    {name: '张三', age: 19, id: '3a62a5ac'},
    {name: '李四', age: 30, id: '9f04ffec'},
  ]
  expect( vio.group(arr, v => v.age) ).toEqual({
    '19': [{name: '张三', age: 19, id: '3a62a5ac'}],
    '30': [{name: '李四', age: 30, id: '9f04ffec'}],
  })
})

test('count', () => {
  const arr = [
    {name: '张三', age: 19, id: '3a62a5ac'},
    {name: '李四', age: 30, id: '9f04ffec'},
    {name: '孙就', age: 30, id: '2c320e2b'},
  ]
  expect( vio.count(arr, v => v.age) ).toEqual({19: 1, 30: 2})
})
